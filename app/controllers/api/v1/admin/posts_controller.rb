class Api::V1::Admin::PostsController < ApplicationController
  before_action :authenticate_user!

  # GET /v1/posts
  def index
    @posts = Post.sorted
    render json: @posts
  end

  # GET /v1/posts/1
  def show
    @post = Post.where(permalink: params[:id]).or(Post.where(id: params[:id])).take
    render json: @post, serializer: PostSerializer, include: :tags
  end
end
