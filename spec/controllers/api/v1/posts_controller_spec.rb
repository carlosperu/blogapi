require 'rails_helper'

# TODO: Fix this tests by taking authentication into account.
RSpec.describe Api::V1::PostsController, type: :controller do

  describe "GET #index" do

  before(:each) do
    4.times { FactoryGirl.create :post }
    get :index
  end

  it "returns 4 post records from the category" do
    posts_response = json_response[:data]
    expect(posts_response).to have(4).posts
  end

  it "returns the attributes object into each post" do
    posts_response = json_response[:data]
    posts_response.each do |post_response|
      expect(post_response[:attributes]).to be_present
    end
  end

  it { should respond_with 200 }
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        tag1 = FactoryGirl.create :tag
        tag2 = FactoryGirl.create :tag
        tags = [ tag1.id, tag2.id ]
        @post_attributes = FactoryGirl.attributes_for :post, tag_ids: tags
        # api_authorization_header user.auth_token
        post :create, params: { post: @post_attributes }
      end

      it "renders the json representation for the post attribute just created" do
        post_response = json_response[:data]
        expect(post_response[:attributes][:title]).to eql @post_attributes[:title]
      end

      it "renders the json array of the tags related to this post" do
        post_response = json_response[:data]
        expect(post_response[:relationships][:tags][:data].size).to eql @post_attributes[:tag_ids].size
      end

      it { should respond_with 201 }
    end

    context "when is not created" do
      before(:each) do
        @invalid_post_attributes = FactoryGirl.attributes_for :post, title: ""
        post :create, params: { post: @invalid_post_attributes }
      end

      it "renders an errors json" do
        post_response = json_response
        expect(post_response).to have_key(:errors)
      end

      it "renders the json errors on why the post could not be created" do
        post_response = json_response
        expect(post_response[:errors][:title]).to include "can't be blank"
      end

      it { should respond_with 422 }
    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @post = FactoryGirl.create :post
    end

    context "when is successfully updated" do
      before(:each) do
        patch :update, params: { id: @post.id,
              post: { title: "Who will be president?" } }
      end

      it "renders the json representation for the updated post" do
        post_response = json_response[:data]
        expect(post_response[:attributes][:title]).to eql "Who will be president?"
      end

      it { should respond_with 200 }
    end

    context "when is not updated" do
      before(:each) do
        patch :update, params: { id: @post.id,
              post: { title: "" } }
      end

      it "renders an errors json" do
        post_response = json_response
        expect(post_response).to have_key(:errors)
      end

      it "renders the json errors on whye the post could not be created" do
        post_response = json_response
        expect(post_response[:errors][:title]).to include "can't be blank"
      end

      it { should respond_with 422 }
    end
  end

end
