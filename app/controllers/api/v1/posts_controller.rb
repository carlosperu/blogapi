class Api::V1::PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_post, only: [:update, :destroy]

  # GET /v1/posts
  def index
    # We filter the index posts list
    @posts = Post.filter(params)
    render json: @posts
  end

  # GET /v1/posts/1
  def show
    @post = Post.permalink_published(params[:id]).take
    render json: @post, include: :tags
  end

  # POST /v1/posts
  def create
    @post = Post.new(post_params)

    if @post.save
      render json: @post, status: :created, location: [:api, :v1, @post]
    else
      render json: { errors: @post.errors }, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/posts/1
  def update
    if @post.update(post_params)
      render json: @post, status: 200, location: [:api, :v1, @post]
    else
      render json: { errors: @post.errors }, status: :unprocessable_entity
    end
  end

  # DELETE /v1/posts/1
  def destroy
    @post.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :content, :published, :locale, tag_ids: [])
    end
end
