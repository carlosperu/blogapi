class Api::V1::TagsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_tag, only: [:update, :destroy]

  # GET /v1/tags
  def index
    @tags = Tag.all
    render json: @tags
  end

  # GET /v1/tags/1
  def show
    @tag = Tag.filter(params)
    render json: @tag, include: ['posts']
  end

  # GET /v1/tags/1/posts
  # Returns a list of posts filtered by the indicated tag
  def posts
    @posts = Post.filter(params).by_tag(params[:id])
    render json: @posts
  end

  # POST /v1/tags
  def create
    @tag = Tag.new(tag_params)

    if @tag.save
      render json: @tag, status: :created, location: [:api, :v1, @tag]
    else
      render json: { errors: @tag.errors }, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/tags/1
  def update
    if @tag.update(tag_params)
      render json: @tag, status: 200, location: [:api, :v1, @tag]
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/tags/1
  def destroy
    @tag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = Tag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tag_params
      params.require(:tag).permit(:name)
    end
end
