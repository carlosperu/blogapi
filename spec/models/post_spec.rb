require 'rails_helper'

RSpec.describe Post, type: :model do
  before { @post = FactoryGirl.build(:post) }

  subject { @post }

  it { should respond_to(:title) }
  it { should respond_to(:content) }
  it { should have_and_belong_to_many(:tags) }

  it { should be_valid }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:content) }
  
end
