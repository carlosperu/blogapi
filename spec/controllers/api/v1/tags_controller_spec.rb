require 'rails_helper'

# TODO: Fix this tests by taking authentication into account.
RSpec.describe Api::V1::TagsController, type: :controller do

  describe "GET #index" do
    before(:each) do
      4.times { FactoryGirl.create :tag }
      get :index
    end

    it "returns 4 records from the database" do
      tags_response = json_response[:data]
      expect(tags_response).to have(4).items
    end

    it "returns the user object into each product" do
      tags_response = json_response[:data]
      tags_response.each do |tag_response|
        expect(tag_response).to be_present
      end
    end

    it { should respond_with 200 }

  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @tag_attributes = FactoryGirl.attributes_for :tag
        # api_authorization_header user.auth_token
        post :create, params: { tag: @tag_attributes }
      end

      it "renders the json representation for the tag record just created" do
        tag_response = json_response[:data]
        expect(tag_response[:attributes][:name]).to eql @tag_attributes[:name]
      end

      it { should respond_with 201 }
    end

    context "when is not created" do
      before(:each) do
        @invalid_tag_attributes = { name: "" }
        post :create, params: { tag: @invalid_tag_attributes }
      end

      it "renders an errors json" do
        tag_response = json_response
        expect(tag_response).to have_key(:errors)
      end

      it "renders the json errors on why the tag could not be created" do
        tag_response = json_response
        expect(tag_response[:errors][:name]).to include "can't be blank"
      end

      it { should respond_with 422 }

    end
  end

end
