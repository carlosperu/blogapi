class User < ActiveRecord::Base
  # Include default devise modules.
  # Notice that :omniauthable is commented out
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable#,
          # :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User
end
