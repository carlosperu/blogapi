class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :published, :permalink, :locale, :published_time
  has_many :tags
end
