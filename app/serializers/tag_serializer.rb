class TagSerializer < ActiveModel::Serializer
  attributes :id, :name, :permalink
  has_many :posts
end
