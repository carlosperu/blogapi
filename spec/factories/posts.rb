FactoryGirl.define do
  factory :post do
    title { Faker::Book.title }
    content { Faker::Hipster.sentence }
    locale { "en" }
    published { Faker::Boolean.boolean(0.5) }
    published_date { Faker::Date.between(Date.today, 1.year.from_now) }
  end
end
