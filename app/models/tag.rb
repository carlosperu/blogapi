class Tag < ApplicationRecord
  has_and_belongs_to_many :posts

  before_save :update_permalinks

  validates :name, presence: true, length: { maximum: 255 }
  validates :permalink, length: { maximum: 255 }

  # Queries all posts filter by given tag and locale
  scope :tag_posts_by_locale, -> (locale, permalink) { includes(:posts).where(posts: { locale: locale }).where(tags: { permalink: permalink }) }

  # List index by locale if the locale param present. List all otherwise.
  def self.filter(params = {})
    tag = params[:locale].present? ? Tag.tag_posts_by_locale(params[:locale], params[:id]) : Tag.where(permalink: params[:id])
  end

  private

    # Establishes the permalink based on the name
    def update_permalinks
      if name.parameterize != permalink
        self.permalink = "#{name.parameterize}"
      end
    end
end
