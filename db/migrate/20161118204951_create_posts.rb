class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :title, limit: 255, null: false
      t.text :content, null: false
      t.string :permalink, limit: 255, index: true
      t.string :locale, null: false
      t.boolean :published, default: false
      t.datetime :published_date, null: true

      t.timestamps
    end
  end
end
