class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :name, limit: 255, null: false
      t.string :permalink, limit: 255, index: true

      t.timestamps
    end

    add_index :tags, :name, unique: true
  end
end
