set :stage, :production

server 'api.polyglotcarlos.com', user: 'deploy', roles: %w{web app db}
