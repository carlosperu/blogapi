class Post < ApplicationRecord
  has_and_belongs_to_many :tags

  before_save :update_published_date
  before_save :update_permalinks

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true
  validates :permalink, length: { maximum: 255 }
  validates :locale, presence: true, length: { maximum: 2 }

  # Sort by published date and updated date
  scope :sorted, -> { order(published_date: :desc, updated_at: :desc) }

  # Filters out unpublished posts
  scope :only_published, -> { where(published: true) }

  # Gets the post with matching permalink only if published
  scope :permalink_published, ->(permalink) { only_published.where(permalink: permalink) }

  # List posts by tag
  scope :by_tag, -> (tag_id) { joins("join posts_tags on posts.id = posts_tags.post_id").where(["posts_tags.tag_id = ?", tag_id]) }

  # List index by locale if the locale param present. List all otherwise.
  def self.filter(params = {})
    posts = params[:locale].present? ? Post.where(locale: params[:locale]) : Post.all
    posts.only_published.sorted
  end

  def published_time
    # Get time in milliseconds unless it's nil
    if self.published_date
      self.published_date.to_datetime.strftime('%Q')
    else
      nil
    end
  end

  private

    # Establishes the permalink based on the title
    def update_permalinks
      if title.parameterize != permalink
        self.permalink = "#{title.parameterize}"
      end
    end

    # Updates published date to now
    def update_published_date
      if self.published
        if self.published_date.nil?
          self.published_date = Time.now
        end
      else
        if !self.published_date.nil?
          self.published_date = nil
        end
      end
    end
end
