Rails.application.routes.draw do
  # The DeviseTokenAuth route helper is skippping mounting the omniauth_callbacks controller because it's disabled
  mount_devise_token_auth_for 'User', at: 'auth', skip: [:omniauth_callbacks]
  # Api definition
  # Defaults to Json, requires an api domain and adds v1 to the path
  namespace :api, defaults: { format: :json },
                    constraints: { subdomain: 'api' }, path: '/'  do
    namespace :v1 do
      resources :tags do
        member do
          get :posts
        end
      end
      resources :posts
      namespace :admin do
        resources :posts, only: [:index, :show]
      end
    end
  end
end
